<?php

    if (isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["confirmpassword"]) && isset($_POST["phone"])){
        
        $name = $_POST["name"];
        $email = $_POST["email"];
        $username = $_POST["username"];
        $password = $_POST["password"];
        $confirmpassword = $_POST["confirmpassword"];
        $gender = $_POST["gender"];
        $phone = $_POST["phone"];
        #Validación de la forma
        if ($name === " ") {
            #Si la forma no es correcta
            $error = "Acceso denegado";
            include("forma_view.html");
        } else {
            #La forma es correcta
            include("article_view.html");
        }
    } else {
        
        include("forma_view.html");
    }

    
?>

