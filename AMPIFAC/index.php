<?php session_start(); ?>
<?php include ("_header.html");?>

<br>

<div class="row">
  <div class="medium-8 columns">
    <p><img src="http://placehold.it/900x450&text=Promoted Article" alt="main article image"></p>
  </div>
  <div class="medium-4 columns">
    <p><img src="http://placehold.it/400x200&text=Other cool article" alt="article promo image" alt="advertisement for deep fried Twinkies"></p>
    <p><img src="http://placehold.it/400x200&text=Other cool article" alt="article promo image"></p>
  </div>
</div>

<hr>

<div class="row column">
  <h4 style="margin: 0;" class="text-center">FOTOS</h4>
</div>

<hr>

<div class="row small-up-3 medium-up-4 large-up-5">

  <div class="column">
    <img src="http://placehold.it/400x370&text=Look at me!" alt="image for article">
  </div>

  <div class="column">
    <img src="http://placehold.it/400x370&text=Look at me!" alt="image for article">
  </div>

  <div class="column">
    <img src="http://placehold.it/400x370&text=Look at me!" alt="image for article">
  </div>

  <div class="column show-for-medium">
    <img src="http://placehold.it/400x370&text=Look at me!" alt="image for article">
  </div>

  <div class="column show-for-large">
    <img src="http://placehold.it/400x370&text=Look at me!" alt="image for article">
  </div>

</div>

<hr>

<div class="row column">
  <h4 style="margin: 0;" class="text-center">SESIONES Y TALLERES</h4>
</div>

<hr>

<div class="row">
  <div class="large-8 columns" style="border-right: 1px solid #E3E5E8;">

  <article>

    <div class="row">
      <div class="large-6 columns">
        <p><img src="http://placehold.it/600x370&text=Look at me!" alt="image for article" alt="article preview image"></p>
      </div>
      <div class="large-6 columns">
        <h5><a href="#">Ejemplo</a></h5>
        <p>
          <span><i class="fi-torso"> By Thadeus &nbsp;&nbsp;</i></span>
          <span><i class="fi-calendar"> 11/23/16 &nbsp;&nbsp;</i></span>
          <span><i class="fi-comments"> 6 inscritos</i></span>
        </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae impedit beatae, ipsum delectus aperiam nesciunt magni facilis ullam.</p>
      </div>
    </div>

    <hr>

    <div class="row">
      <div class="large-6 columns">
        <p><img src="http://placehold.it/600x370&text=Look at me!" alt="image for article" alt="article preview image"></p>
      </div>
      <div class="large-6 columns">
        <h5><a href="#">Ejemplo 2</a></h5>
        <p>
          <span><i class="fi-torso"> By Thadeus &nbsp;&nbsp;</i></span>
          <span><i class="fi-calendar"> 11/23/16 &nbsp;&nbsp;</i></span>
          <span><i class="fi-comments"> 4 inscritos</i></span>
        </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae impedit beatae, ipsum delectus aperiam nesciunt magni facilis ullam.</p>
      </div>
    </div>

    <hr>

    <div class="row">
      <div class="large-6 columns">
        <p><img src="http://placehold.it/600x370&text=Look at me!" alt="image for article" alt="article preview image"></p>
      </div>
      <div class="large-6 columns">
        <h5><a href="#">Ejemplo 3</a></h5>
        <p>
          <span><i class="fi-torso"> By Thadeus &nbsp;&nbsp;</i></span>
          <span><i class="fi-calendar"> 11/23/16 &nbsp;&nbsp;</i></span>
          <span><i class="fi-comments"> 6 inscritos</i></span>
        </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae impedit beatae, ipsum delectus aperiam nesciunt magni facilis ullam.</p>
      </div>
    </div>

    <hr>

    <div class="row">
      <div class="large-6 columns">
        <p><img src="http://placehold.it/600x370&text=Look at me!" alt="image for article" alt="article preview image"></p>
      </div>
      <div class="large-6 columns">
        <h5><a href="#">Astronaut's Watch Worn on the Moon Sells for Record $1.6 Million</a></h5>
        <p>
          <span><i class="fi-torso"> By Thadeus &nbsp;&nbsp;</i></span>
          <span><i class="fi-calendar"> 11/23/16 &nbsp;&nbsp;</i></span>
          <span><i class="fi-comments"> 6 comments</i></span>
        </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae impedit beatae, ipsum delectus aperiam nesciunt magni facilis ullam.</p>
      </div>
    </div>

    <hr>

    <div class="row">
      <div class="large-6 columns">
        <p><img src="http://placehold.it/600x370&text=Look at me!" alt="image for article" alt="article preview image"></p>
      </div>
      <div class="large-6 columns">
        <h5><a href="#">Interstellar Dust on the Galaxy's Magnetic Field | Space Wallpaper</a></h5>
        <p>
          <span><i class="fi-torso"> By Thadeus &nbsp;&nbsp;</i></span>
          <span><i class="fi-calendar"> 11/23/16 &nbsp;&nbsp;</i></span>
          <span><i class="fi-comments"> 6 comments</i></span>
        </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae impedit beatae, ipsum delectus aperiam nesciunt magni facilis ullam.</p>
      </div>
    </div>

    <hr>

    <div class="row">
      <div class="large-6 columns">
        <p><img src="http://placehold.it/600x370&text=Look at me!" alt="image for article" alt="article preview image"></p>
      </div>
      <div class="large-6 columns">
        <h5><a href="#">Explore the Moon (Virtually) with These Awesome Global Maps</a></h5>
        <p>
          <span><i class="fi-torso"> By Thadeus &nbsp;&nbsp;</i></span>
          <span><i class="fi-calendar"> 11/23/16 &nbsp;&nbsp;</i></span>
          <span><i class="fi-comments"> 6 comments</i></span>
        </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae impedit beatae, ipsum delectus aperiam nesciunt magni facilis ullam.</p>
      </div>
    </div>

    <hr>

    <div class="row">
      <div class="large-6 columns">
        <p><img src="http://placehold.it/600x370&text=Look at me!" alt="image for article" alt="article preview image"></p>
      </div>
      <div class="large-6 columns">
        <h5><a href="#">Best Space Books and Sci-Fi: A Space.com Reading List</a></h5>
        <p>
          <span><i class="fi-torso"> By Thadeus &nbsp;&nbsp;</i></span>
          <span><i class="fi-calendar"> 11/23/16 &nbsp;&nbsp;</i></span>
          <span><i class="fi-comments"> 6 comments</i></span>
        </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae impedit beatae, ipsum delectus aperiam nesciunt magni facilis ullam.</p>
      </div>
    </div>

    <hr>

 

    </article>

  </div>

  <div class="large-4 columns">

    <aside>

     

      <br>


      <br>

<div class="fb-like-box fb_iframe_widget" data-href="https://www.facebook.com/Ampifac-IAP-271859959552291/?fref=ts" data-width="320" data-height="470" data-show-faces="false" data-border-color="#D6BCEB" data-stream="true" data-header="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=320&amp;header=false&amp;height=470&amp;href=https://www.facebook.com/Ampifac-IAP-271859959552291/?fref=ts;locale=es_LA&amp;sdk=joey&amp;show_faces=false&amp;stream=true&amp;width=320"><span style="vertical-align: bottom; width: 320px; height: 470px;"><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAmpifac-IAP-271859959552291%2F%3Ffref%3Dts&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></span></div>
		
    </aside>
    
  </div>
</div>

<?php include("_footer.html"); ?>